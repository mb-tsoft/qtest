
Feature:
  Como Mesero, deseo atender el pedido de una mesa para que el cliente sepa que ya se le tomó la orden

  Scenario: Para todos los escenarios
    Given   que ya se haya entregado a cocina el pedido
    When    oprima el botón “despachar pedido”
    Then    se le envía una notificación a la mesa y se marcan los productos como despachados
