package Tsoft.steps;

import Tsoft.objects.oRegistrar;
import net.thucydides.core.annotations.Step;

public class sReutilizable {
	
	oRegistrar oRegis;
	
	public void url() {
		oRegis.open();	
	}
	
	public void login(String userName, String password) {
		oRegistrar.txtUser.sendKeys(userName);
		oRegistrar.txtPass.sendKeys(password);	
		oRegistrar.btnLogin.click();
		
	}
	@Step
	public void closeSession(String obj, String msj) {
		oRegistrar.logOut.click();
		msj = oRegistrar.msjLogOut.getText();
		if (msj.equals("You have been logged out. See you back soon.")) {
			System.out.println("El script paso correctamente");
		}
	}
	
	@Step
	public void mensajePantalla (String dato) {}
	
}

