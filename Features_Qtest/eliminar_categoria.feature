
Feature:
  Como Gerente, deseo eliminar categoría de productos para organizar mejor los productos que ofrezco

  Scenario: Hay categorías
    Given   que este en el menú del restaurante
    When    elija una categoría y de en el botón eliminar
    Then    se da una notificación de eliminar y si se confirma se elimina la categoría del menú del restaurante
    
  Scenario: No hay categorías
    Given   que no hay categorías en el menú
    When    se de en el botón eliminar
    Then    el botón se deshabilita
    
