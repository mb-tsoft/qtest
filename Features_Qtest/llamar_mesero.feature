
Feature:
  Como Cliente, deseo llamar a un mesero para pedir ayuda o asesoría en la mesa

  Scenario: Para todos los escenarios
    Given   que esté en cualquier parte de la aplicación
    When    dés al botón de llamar mesero
    Then    se muestra una notificación a los meseros avisándoles que en esta mesa en específico necesitan a un mesero