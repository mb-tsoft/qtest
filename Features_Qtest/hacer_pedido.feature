
Feature:
  Como Cliente, deseo hacer pedido para poder comer los platos que pedí

  Scenario: Hay uno o mas productos en la orden
    Given   que estoy en la lista del pedido
    When    dé el botón de “ordenar”
    Then    se envía la notificación a los meseros del pedido con el pedido de la mesa
    
  Scenario: No hay productos en la orden
    Given   que no hay productos en la orden
    When    se oprima el botón de hacer pedido
    Then    se saca un mensaje que diga que no se puede hacer un pedido porque no hay platos en la orden
    
  Scenario: Ya se pidió la cuenta
    Given   que ya se pidió la cuenta
    When    se de en el botón hacer pedido
    Then    este botón de pedido se bloquea y se pone un mensaje diciendo que ya el pedido esta por pagarse
    
  Scenario: Hay productos sin despachar
    Given   que haya uno o más productos sin despachar
    When    se oprima el botón de pedido
    Then    se notifica del pedido nuevamente a los meseros
    