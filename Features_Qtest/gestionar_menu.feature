Feature:
  Como Gerente, deseo crear un menú para ofrecerle los productos a mis clientes.

  Scenario: No hay un menú creado
    Given   que este en la pantalla de gerente 
        And No haya un menú creado
    When    ingrese a gestionar menú
    Then    se crea un menú básico con 2 categorías comidas bebidas
  
  Scenario: Ya hay un menú creado
    Given   que este en la pantalla de gerente 
        And ya hay un menú creado
    When    ingrese a gestionar menú
    Then    se muestra el menú que ya existe
  
