package Tsoft.objects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://testphp.vulnweb.com/login.php")

public class oRegistrar extends PageObject {
	
	//LOGIN

	@FindBy(xpath = "//*[@id=\"content\"]/div[1]/form/table/tbody/tr[1]/td[2]/input")
	public static WebElementFacade txtUser;
	
	@FindBy(xpath = "//*[@id=\"content\"]/div[1]/form/table/tbody/tr[2]/td[2]/input")
	public static WebElementFacade txtPass;
	
	@FindBy(xpath = "//*[@id=\"content\"]/div[1]/form/table/tbody/tr[3]/td/input")
	public static WebElementFacade btnLogin;
	
	@FindBy(id = "bootstrap-admin-template")
	public WebElementFacade LblHome;
	
	//ANOTHER
	
	@FindBy(xpath = "//*[@id=\"globalNav\"]/table/tbody/tr/td[1]/a[3]")
	public static WebElementFacade btnArtist;
	
	@FindBy(xpath = "//*[@id=\"content\"]/div[1]/a/h3")
	public static WebElementFacade imgArtist;
	
	//LOGOUT
	
	@FindBy(xpath = "//*[@id=\"globalNav\"]/table/tbody/tr/td[2]/a")
	public static WebElementFacade logOut;
	
	@FindBy(xpath = "//*[@id='pageName']")
	public static WebElementFacade msjLogOut;
	
	@FindBy(xpath = "//*[@id='content']/div/p[2]")
	public static WebElementFacade pruebaMensaje;

}


