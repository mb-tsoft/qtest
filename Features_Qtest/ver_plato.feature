
Feature:
  Como Cliente, deseo ver detalles de un plato para poder elegir la comida que voy a pedir

  Scenario: Hay productos en categoría
    Given   Que hay productos en una categoría
    When    esté navegando la lista de productos
    Then    debo poder ver la información detallada del plato (nombre, precio y descripción)
