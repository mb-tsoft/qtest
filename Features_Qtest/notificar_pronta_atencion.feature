
Feature:
  Como Cliente, deseo ver si el mesero recibió mi llamado para saber que me van a atender pronto

  Scenario: Para todos los escenarios
    Given   que haya llamado un mesero
    When    el mesero acepte la notificación
    Then    se mostrara una notificación en la mesa que dice que el mesero los atenderá pronto
