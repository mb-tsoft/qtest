
Feature:
  Como Mesero, deseo ver el tiempo que lleva una mesa esperando atención para saber que clientes debo atender primero

  Scenario: Para todos los escenarios
    Given   que esté en la pantalla de atención de mesas
        And haya una notificación de llamado de una mesa
    When    se alerte
    Then    se muestre un cronometro que vaya contando el tiempo desde que se generó el llamado
